﻿using Portal_Empreendedor.Entidades;
using Portal_Empreendedor.Models;
using PortalEmpreendedor.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Portal_Empreendedor.Controllers
{
    public class AcessoController : Controller
    {
        private DB_PORTAL_EMPREENDEDORContext banco;
        private Perfil_Empreendedor perfilEmpreendedor;
        private Perfil_Investidor perfilInvestidor;

        public AcessoController()
        {
            banco = new DB_PORTAL_EMPREENDEDORContext();
        }
        //
        // GET: /Acesso/
        public ActionResult Login()
        {
            return View();
        }



        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Login(acesso acesso)
        {

            if (string.IsNullOrEmpty(acesso.email))
                ModelState.AddModelError("usuario", "Campo e-mail obrigatório!");
            if (string.IsNullOrEmpty(acesso.senha))
                ModelState.AddModelError("senha", "Campo senha obrigatório!");
            if (!ModelState.IsValid)
                return View(acesso);

            var empreendedor = banco.empreendedors.FirstOrDefault(x => x.email.ToLower().Equals(acesso.email.ToLower()));
            var investidor = banco.investidors.FirstOrDefault(x => x.email.ToLower().Equals(acesso.email.ToLower()));

            if (empreendedor == null && investidor == null)
            {
                ModelState.AddModelError("", "Usuário não encontrado");
                return View(acesso);
            }

            var senhaCript = Cripitografia.Criptografar(acesso.senha);

            if (empreendedor != null)
            {
                var senhaEmpreendedor = banco.acessoes.Any(x => x.email == empreendedor.email && x.senha == senhaCript);

                if (senhaEmpreendedor)
                {
                    string id = Cripitografia.Criptografar(Convert.ToString(empreendedor.id));

                    return RedirectToAction("Index", "PerfilEmpreendedor", new { empreendedor = id });
                }
                else
                {
                    ModelState.AddModelError("", "Senha Incorreta");
                    return View(acesso);
                }

            }
            else
            {
                var senhaInvestidor = banco.acessoes.Any(x => x.email == investidor.email && x.senha == senhaCript);

                if (senhaInvestidor)
                {
                    string id = Cripitografia.Criptografar(Convert.ToString(investidor.id));
                    return RedirectToAction("Index", "PerfilInvestidor", new { investidor = id });
                }
                else
                {
                    ModelState.AddModelError("", "Senha Incorreta");
                    return View(acesso);
                }


            }

        }
    }
}