﻿using Portal_Empreendedor.Models;
using PortalEmpreendedor.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal_Empreendedor.Controllers
{
    public class EmpreendedorController : Controller
    {
        private DB_PORTAL_EMPREENDEDORContext banco;
        private acesso acesso;
        private bool verificaPassword;

        public EmpreendedorController(){

            this.banco = new DB_PORTAL_EMPREENDEDORContext();
        } 
        //
        // GET: /Empreendedor/
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(empreendedor empreend)
        {
            if (ModelState.IsValid)
            {
                this.verificaPassword = this.Password(empreend);

                if (this.verificaPassword)
                {
                    banco.empreendedors.Add(empreend);
                    banco.SaveChanges();
                    return RedirectToAction("Login", "Acesso");
                }
                else
                {
                    return View(empreend);
                }

            }

            return View(empreend);
        }
        private Boolean Password(empreendedor empreendedor)
        {
            if (empreendedor != null)
            {
                this.acesso = new acesso();
                acesso.id = empreendedor.id;
                acesso.senha = Cripitografia.Criptografar(empreendedor.password);
                acesso.email = empreendedor.email;

                banco.Database.Log = Console.Write;
                bool verifica = this.verificaEmail(empreendedor);

                if (verifica)
                {
                    banco.acessoes.Add(acesso);
                    return true;

                }
            }

            return false;

        }

        private bool verificaEmail(empreendedor empreendedor)
        {
            bool result;
            if (empreendedor != null)
            {
                result = banco.acessoes.Any(x => x.email.ToLower().Equals(empreendedor.email.ToLower()));
            }
            else
            {
                return false;
            }

            if (result)
            {
                ModelState.AddModelError("email", "E-mail já cadastrado no sistema!");
                return false;
            }
            else
            {
                return true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                banco.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}