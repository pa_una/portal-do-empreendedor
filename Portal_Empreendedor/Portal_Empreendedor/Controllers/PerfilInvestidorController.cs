﻿using Portal_Empreendedor.Entidades;
using Portal_Empreendedor.Models;
using PortalEmpreendedor.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalEmpreendedor.Controllers
{
    public class PerfilInvestidorController : Controller
    {
        private DB_PORTAL_EMPREENDEDORContext banco;
        private investidor perfil;

        public PerfilInvestidorController()
        {
            this.banco = new DB_PORTAL_EMPREENDEDORContext();
            this.perfil = new investidor();
        }

        //
        // GET: /PerfilInvestidor/
        public ActionResult Index(string investidor)
        {
            if (investidor == "")
            {
                return RedirectToAction("Login", "Acesso");
            }

            string idString = Cripitografia.Descriptografar(investidor);
            int id = Convert.ToInt32(idString);
            var dadosInvestidor = buscaDadosInvstidor(id);

            if (dadosInvestidor == null)
            {
                return Content("Item not found");
            }

            return View(dadosInvestidor);
        }

        private investidor buscaDadosInvstidor(int id)
        {
            this.perfil = banco.investidors.FirstOrDefault(x => x.id == id);
            if(this.perfil == null){
                return null;
            }
            return this.perfil;
        }
	}
}