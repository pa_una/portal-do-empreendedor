﻿using Portal_Empreendedor.Models;
using PortalEmpreendedor.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal_Empreendedor.Controllers
{
    public class InvestidorController : Controller
    {
        private DB_PORTAL_EMPREENDEDORContext banco;
        private acesso acesso;

        public InvestidorController()
        {
            this.banco = new DB_PORTAL_EMPREENDEDORContext();
        }
        //
        // GET: /Investidor/
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(investidor invest)
        {
            if (ModelState.IsValid)
            {
                bool verificaPassword = this.Password(invest);

                if (verificaPassword)
                {
                    banco.investidors.Add(invest);
                    banco.SaveChanges();
                    return RedirectToAction("Login", "Acesso");
                }
                else
                {
                    return View(invest);
                }
            }
            return HttpNotFound();
        }


        private Boolean Password(investidor invest)
        {
            if (invest != null)
            {
                this.acesso = new acesso();
                acesso.id = invest.id;
                acesso.senha = Cripitografia.Criptografar(invest.password);
                acesso.email = invest.email;

                banco.Database.Log = Console.Write;
                bool verifica = this.verificaEmail(invest);

                if (verifica)
                {
                    banco.acessoes.Add(acesso);
                    return true;

                }
            }

            return false;

        }

        private bool verificaEmail(investidor investidor)
        {
            bool result;
            if (investidor != null)
            {
                result = banco.acessoes.Any(x => x.email.ToLower().Equals(investidor.email.ToLower()));
            }
            else
            {
                return false;
            }

            if (result)
            {
                ModelState.AddModelError("email", "E-mail já cadastrado no sistema!");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}