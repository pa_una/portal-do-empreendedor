﻿using Portal_Empreendedor.Entidades;
using Portal_Empreendedor.Models;
using PortalEmpreendedor.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalEmpreendedor.Controllers
{
    public class PerfilEmpreendedorController : Controller
    {
        private DB_PORTAL_EMPREENDEDORContext banco;
        private empreendedor perfil;

        public  PerfilEmpreendedorController()
        {
            this.banco = new DB_PORTAL_EMPREENDEDORContext();
            this.perfil = new empreendedor();
        }

        //
        // GET: /PerfilEmpreendedor/
        public ActionResult Index(string empreendedor)
        {
            if (empreendedor == null)
            {
                return RedirectToAction("Login", "Acesso");
            }

            string idString = Cripitografia.Descriptografar(empreendedor);
            int id = Convert.ToInt32(idString);
            var dadosEmpreendedor = buscaDadosEmpreendedor(id);

            if (dadosEmpreendedor == null)
            {
                return Content("Item not found");
            }

            return View(dadosEmpreendedor);
        }

        private empreendedor buscaDadosEmpreendedor(int id)
        {
            this.perfil = banco.empreendedors.FirstOrDefault(x => x.id == id);

            if (this.perfil == null)
            {
                return null;
            }
            return this.perfil;
        }

	}
}