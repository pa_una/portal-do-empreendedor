using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal_Empreendedor.Models
{
    public partial class investidor
    {
        public investidor()
        {
            this.areas = new List<area>();
            this.tipo = 1;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required(ErrorMessage = "Voc� deve inserir um nome!")]
        public string nome { get; set; }
        [Required(ErrorMessage = "Voc� deve inserir um CPF ou CNPJ!")]
        public string cpf_cnpj { get; set; }
        public int tipo { get; set; }
        [Required(ErrorMessage = "Voc� deve inserir um descri��o sobre o que voc� busca!")]
        public string descricao { get; set; }
        [Required(ErrorMessage = "Voc� deve inserir um endere�o!")]
        public string endereco { get; set; }
        [Required(ErrorMessage = "Voc� deve inserir um n�mero de telefone!")]
        public string telefone { get; set; }
        [Required(ErrorMessage = "Voc� deve inserir um n�mero de celular!")]
        public string celular { get; set; }
        [Required(ErrorMessage = "Voc� deve inserir um email!")]
        public string email { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Voc� deve inserir um password!")]
        public string password { get; set; }
        public virtual ICollection<area> areas { get; set; }
    }
}
