using System;
using System.Collections.Generic;

namespace Portal_Empreendedor.Models
{
    public partial class empreendimento
    {
        public int id { get; set; }
        public string descricao { get; set; }
        public string resumo { get; set; }
        public int id_area { get; set; }
        public int id_empreendedor { get; set; }
        public virtual area area { get; set; }
        public virtual empreendedor empreendedor { get; set; }
    }
}
