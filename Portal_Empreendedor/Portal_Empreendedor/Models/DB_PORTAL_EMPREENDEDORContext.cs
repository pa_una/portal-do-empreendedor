using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Portal_Empreendedor.Models.Mapping;

namespace Portal_Empreendedor.Models
{
    public partial class DB_PORTAL_EMPREENDEDORContext : DbContext
    {
        static DB_PORTAL_EMPREENDEDORContext()
        {
            Database.SetInitializer<DB_PORTAL_EMPREENDEDORContext>(null);
        }

        public DB_PORTAL_EMPREENDEDORContext()
            : base("Name=DB_PORTAL_EMPREENDEDORContext")
        {
        }

        public DbSet<acesso> acessoes { get; set; }
        public DbSet<area> areas { get; set; }
        public DbSet<empreendedor> empreendedors { get; set; }
        public DbSet<empreendimento> empreendimentoes { get; set; }
        public DbSet<investidor> investidors { get; set; }
        public DbSet<tipo_investidor> tipo_investidor { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new acessoMap());
            modelBuilder.Configurations.Add(new areaMap());
            modelBuilder.Configurations.Add(new empreendedorMap());
            modelBuilder.Configurations.Add(new empreendimentoMap());
            modelBuilder.Configurations.Add(new investidorMap());
            modelBuilder.Configurations.Add(new tipo_investidorMap());
        }
    }
}
