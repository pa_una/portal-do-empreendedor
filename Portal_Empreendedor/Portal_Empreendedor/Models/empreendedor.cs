using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal_Empreendedor.Models
{
    public partial class empreendedor
    {
        public empreendedor()
        {
            this.empreendimentoes = new List<empreendimento>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required(ErrorMessage="Voc� deve inserir um nome!")]
        public string nome { get; set; }

        [Required(ErrorMessage = "Voc� deve inserir um CPF ou CNPJ!")]
        public string cpf_cnpj { get; set; }

        [Required(ErrorMessage = "Voc� deve inserir um telefone!")]
        public string telefone { get; set; }

        [Required(ErrorMessage = "Voc� deve inserir um n�mero de celular!")]
        public string celular { get; set; }

       [Required(ErrorMessage = "Voc� deve inserir um e-mail!")]
        public string email { get; set; }

        [Required(ErrorMessage = "Voc� deve inserir um descri��o sobre voc�!")]
        public string descricao { get; set; }
        
        [NotMapped]
        public string password { get; set; }

        public virtual ICollection<empreendimento> empreendimentoes { get; set; }
    }
}
