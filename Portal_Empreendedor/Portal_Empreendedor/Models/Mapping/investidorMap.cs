using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Portal_Empreendedor.Models.Mapping
{
    public class investidorMap : EntityTypeConfiguration<investidor>
    {
        public investidorMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.nome)
                .HasMaxLength(255);

            this.Property(t => t.cpf_cnpj)
                .IsFixedLength()
                .HasMaxLength(18);

            this.Property(t => t.descricao)
                .HasMaxLength(255);

            this.Property(t => t.endereco)
                .HasMaxLength(255);

            this.Property(t => t.telefone)
                .IsFixedLength()
                .HasMaxLength(15);

            this.Property(t => t.celular)
                .IsFixedLength()
                .HasMaxLength(15);

            this.Property(t => t.email)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("investidor");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.cpf_cnpj).HasColumnName("cpf_cnpj");
            this.Property(t => t.tipo).HasColumnName("tipo");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.endereco).HasColumnName("endereco");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.celular).HasColumnName("celular");
            this.Property(t => t.email).HasColumnName("email");
        }
    }
}
