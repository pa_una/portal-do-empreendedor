using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Portal_Empreendedor.Models.Mapping
{
    public class areaMap : EntityTypeConfiguration<area>
    {
        public areaMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.descricao)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("area");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.descricao).HasColumnName("descricao");

            // Relationships
            this.HasMany(t => t.investidors)
                .WithMany(t => t.areas)
                .Map(m =>
                    {
                        m.ToTable("area_investidor");
                        m.MapLeftKey("id_area");
                        m.MapRightKey("id_investidor");
                    });


        }
    }
}
