using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Portal_Empreendedor.Models.Mapping
{
    public class tipo_investidorMap : EntityTypeConfiguration<tipo_investidor>
    {
        public tipo_investidorMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.descricao)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("tipo_investidor");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.descricao).HasColumnName("descricao");
        }
    }
}
