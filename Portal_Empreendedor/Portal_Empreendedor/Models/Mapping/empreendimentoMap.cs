using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Portal_Empreendedor.Models.Mapping
{
    public class empreendimentoMap : EntityTypeConfiguration<empreendimento>
    {
        public empreendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.descricao)
                .HasMaxLength(255);

            this.Property(t => t.resumo)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("empreendimento");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.resumo).HasColumnName("resumo");
            this.Property(t => t.id_area).HasColumnName("id_area");
            this.Property(t => t.id_empreendedor).HasColumnName("id_empreendedor");

            // Relationships
            this.HasRequired(t => t.area)
                .WithMany(t => t.empreendimentoes)
                .HasForeignKey(d => d.id_area);
            this.HasRequired(t => t.empreendedor)
                .WithMany(t => t.empreendimentoes)
                .HasForeignKey(d => d.id_empreendedor);

        }
    }
}
