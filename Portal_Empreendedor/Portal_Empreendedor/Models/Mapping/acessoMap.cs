using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Portal_Empreendedor.Models.Mapping
{
    public class acessoMap : EntityTypeConfiguration<acesso>
    {
        public acessoMap()
        {
            // Primary Key
            this.HasKey(t => t.email);

            // Properties
            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.senha)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("acesso");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.senha).HasColumnName("senha");
        }
    }
}
