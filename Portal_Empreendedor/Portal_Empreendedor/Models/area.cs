using System;
using System.Collections.Generic;

namespace Portal_Empreendedor.Models
{
    public partial class area
    {
        public area()
        {
            this.empreendimentoes = new List<empreendimento>();
            this.investidors = new List<investidor>();
        }

        public int id { get; set; }
        public string descricao { get; set; }
        public virtual ICollection<empreendimento> empreendimentoes { get; set; }
        public virtual ICollection<investidor> investidors { get; set; }
    }
}
