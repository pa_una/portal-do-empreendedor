﻿

CREATE TABLE empreendedor
(
	id int IDENTITY PRIMARY KEY,
	nome varchar(255),
	cpf_cnpj char(18),
	telefone char(15),
	celular char(15),
	email varchar(255),
	descricao varchar(255)
);

CREATE TABLE acesso
(
	id int,
	email varchar(50) not null,
	senha varchar (255)
CONSTRAINT PK_ACESSO PRIMARY KEY (email)
);

CREATE TABLE investidor 
(
	id int IDENTITY PRIMARY KEY,
	nome varchar(255),
	cpf_cnpj char(18),
	tipo int,
	descricao varchar (255),
	endereco varchar (255),
	telefone char(15),
	celular char(15),
	email varchar(255));

CREATE TABLE area 
(
	id int IDENTITY PRIMARY KEY,
	descricao varchar(255)
);

CREATE TABLE empreendimento 
(
	id int IDENTITY PRIMARY KEY,
	descricao varchar(255),
	resumo varchar(255),
	id_area int not null,
	id_empreendedor int not null,

CONSTRAINT FK_AREA FOREIGN KEY (id_area) REFERENCES area(id),
CONSTRAINT FK_EMPREENDEDOR FOREIGN KEY (id_empreendedor) REFERENCES empreendedor(id));

CREATE TABLE tipo_investidor 
(
	id int IDENTITY PRIMARY KEY,
	descricao varchar(255)
);

CREATE TABLE area_investidor 
(
	id_investidor int not null,
	id_area int not null,
CONSTRAINT FK_INVESTIDOR FOREIGN KEY (id_investidor) REFERENCES investidor(id),
CONSTRAINT FK_AREA_INVESTIDOR FOREIGN KEY (id_area) REFERENCES area(id));