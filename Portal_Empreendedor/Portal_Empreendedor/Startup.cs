﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Portal_Empreendedor.Startup))]
namespace Portal_Empreendedor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
